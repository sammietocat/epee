/**
*@file  consoleHandlerBinderDemo.cpp
*@brief     a example demonstrating the usage of the
        {@link epee::console_handlers_binder} class
*@version v1.0, 2017-07-08
*@author   sammietocat
*/
#include <iostream>

#include "console_handler.h"

using namespace std;

/**
*@brief print a 'hello whos[0] ... whos[n]' string to stdout
*@param whos    persons' name to say hello
*@return `true` dummy
*@note to become a eligible function for `epee::console_handlers_binder`
        the signature must be the same the declaration as follows,
        where the only parameter is a vector, and return value is a
        boolean value indicating the status of execution
*/
bool sayHelloTo( vector<string> whos );

int main( int argc, char *argv[] ) {
    const string                  PROMPT = "sammy";
    epee::console_handlers_binder hdlb;

    hdlb.set_handler( "say", boost::bind( sayHelloTo, _1 ),
                      "command to say hello to someone" );
    // print the usage string of the program
    cout << hdlb.get_usage() << endl;
    // start a thread to handle the command
    // note the ownership are handed over to handling thread
    // until the thread join
    if ( !hdlb.run_handling( PROMPT, "bla, bla..." ) ) {
        cerr << "error during handling" << endl;
    }
    // stop handling manually if wanted.
    // Otherwise, we can also exit by command `exit` or `q` command
    // hdlb.stop_handling();

    return 0;
}

bool sayHelloTo( vector<string> whos ) {
    cout << "hello ";
    for ( string p : whos ) {
        cout << p << " ";
    }
    cout << endl;
    return true;
}