//
// Created by loccs on 7/21/17.
//

#ifndef LEVINSRV_HPP
#define LEVINSRV_HPP

#include <iostream>
#include "net/levin_base.h"
#include "net/levin_server_cp2.h"
#include "storages/levin_abstract_invoke2.h"

#include "Cmd.hpp"

//using namespace epee;
using namespace std;

namespace enu = epee::net_utils;

namespace sammy {
    /**
     * @brief   a wrapper around the
     *          {@link epee::levin::levin_commands_handler<epee::net_utils::connection_context_base>},
     *          as a handler
     * */
    class LevinSrv : public epee::levin::levin_commands_handler<> {
    public:
        //typedef epee::net_utils::connection_context_base connCtx;

        LevinSrv(const int thCnt = 4);

        ~LevinSrv();

        bool bind2Address(const string &ip, const string &port);

        bool start();

        void stop();

        bool finalize();

        void communicate() {
            const string srvIP = "127.0.0.1";
            const string srvPORT = "9090";

            enu::connection_context_base connCtx = boost::value_initialized<decltype(connCtx)>();
            if (!worker.connect(srvIP, srvPORT, 10000, connCtx)) {
                cerr << "connection failure" << endl;
                return;
            }
            cout << "connect to " << srvIP << ":" << srvPORT << " ok" << endl;

            sCmd::request req;
            req.info = "sammy";
            sCmd::response resp = boost::value_initialized<decltype(resp)>();
            if (!enu::invoke_remote_command2(connCtx.m_connection_id, sCmd::ID, req, resp,
                                             worker.get_config_object())) {
                cerr << "failed to invoke remove command" << endl;
                return;
            }
            cout << "response is " << endl;
            cout << "\tstatus: " << resp.ok << endl;
            cout << "\tfeedback: " << resp.feedback << endl;

            worker.get_config_object().close(connCtx.m_connection_id);
        }

        void communicateAsync() {
            const string srvIP = "127.0.0.1";
            const string srvPORT = "9090";

            //enu::connection_context_base connCtx = boost::value_initialized<decltype(connCtx)>();
            const int cmdID = sCmd::ID;
            sCmd::request req;
            req.info = "sammy-async";
            //sCmd::response resp = boost::value_initialized<decltype(resp)>();

            worker.connect_async(srvIP, srvPORT, 10000,
                                 [this, cmdID, req, srvIP, srvPORT](
                                         enu::connection_context_base &connCtx, const boost::system::error_code &ec_) {
                                     if (!!ec_) {
                                         cerr << "failed to connect to " << srvIP << ":" << srvPORT << endl;
                                     } else {//connected ok!
                                         enu::async_invoke_remote_command2<sCmd::response>(
                                                 connCtx.m_connection_id, cmdID, req, worker.get_config_object(),
                                                 [this, srvIP, srvPORT](int res_code, sCmd::response &resp,
                                                                         enu::connection_context_base &connCtx) {
                                                     if (res_code < 0) {
                                                         cerr << "failed to invoke " << srvIP << ":" << srvPORT << endl;
                                                     } else {//invoked ok
                                                         cout << "response is " << endl;
                                                         cout << "\tstatus: " << resp.ok << endl;
                                                         cout << "\tfeedback: " << resp.feedback << endl;
                                                     }
                                                     worker.get_config_object().close(connCtx.m_connection_id);
                                                     return true;
                                                 });
                                         cout << "Client sCmd async invoke requested" << endl;
                                     }
                                 });
        }

    private:
        const int THREAD_COUNT;

        enu::boosted_levin_async_server worker; ///< object doing the actual heavy lifting job
        //std::atomic<bool> running;  ///< switch controlling the running status of the server

        // interfaces inherited to be implement
        int
        invoke(int command, const std::string &in_buff, std::string &buff_out, enu::connection_context_base &context);

        int notify(int command, const std::string &in_buff, enu::connection_context_base &context);

        // handler functions
        template<class t_context>
        int handle_invoke_map(bool is_notify, int command, const std::string &in_buff, std::string &buff_out,
                              t_context &context, bool &handled);

        // command handlers
        int sayHello(int cmdId, sCmd::request &req, sCmd::response &resp, const enu::connection_context_base &ctx);
        int wakeUp(int cmdId, sCmd::request &req, const enu::connection_context_base &ctx);
    };
}

#endif //LEVINSRV_HPP
