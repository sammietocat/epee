// Copyright (c) 2006-2013, Andrey N. Sabelnikov, www.sabelnikov.net
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// * Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer.
// * Redistributions in binary form must reproduce the above copyright
// notice, this list of conditions and the following disclaimer in the
// documentation and/or other materials provided with the distribution.
// * Neither the name of the Andrey N. Sabelnikov nor the
// names of its contributors may be used to endorse or promote products
// derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER  BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 

/**
 * @file    misc_language.h
 * @brief   a set of utilities for  (all are in namespace `epee`)
 *              + exception handling:  {@link STD_TRY_BEGIN()}, {@link STD_TRY_CATCH(where_, ret_val)}
 *              + default value maker: {@link AUTO_VAL_INIT(v)}
 *              + stringfier: {@link STRINGIFY(s)}, {@link STRINGIFY_EXPAND(s)}
 *              + statistics handling: {@link misc_utils::get_max_t_val}, {@link misc_utils::median}
 *              + iterator mover: {@link misc_utils::move_it_forward}, {@link misc_utils::move_it_backward}
 *              + less-than comparator: {@link misc_utils::less_as_pod}, {@link misc_utils::is_less_as_pod}
 *              + thread sleeper: {@link misc_utils::sleep_no_w}
 *              + scope leaving callback wrapper: {@link misc_utils::call_befor_die}
 *              + element eraser: {@link misc_utils::erase_if()}
 */

#pragma once

#include <limits>
#include <boost/thread.hpp>
#include <boost/utility/value_init.hpp>

namespace epee {
    /**
     * @brief   begin a try block
     */
#define STD_TRY_BEGIN() try {

    /**
     * @brief   catch the error and print the corresponding message,
     *          signal where the exception occurs based on `where_` and return the
     *          given error code specified by `ret_val`
     */
#define STD_TRY_CATCH(where_, ret_val) \
    } \
    catch (const std::exception  &e) \
    { \
        LOG_ERROR("EXCEPTION: " << where_  << ", mes: "<< e.what());  \
        return ret_val; \
    } \
    catch (...) \
    { \
        LOG_ERROR("EXCEPTION: " << where_ ); \
        return ret_val; \
    }


    /**
     * @brief helper class, to make able get namespace via decltype()::
     * */
    template<class base_class>
    class namespace_accessor : public base_class {
    };

    /**
     * @brief   make a default value for a given value
     * */
#define AUTO_VAL_INIT(v)   boost::value_initialized<decltype(v)>()

    /**
     * @brief alias for {@link STRINGIFY}
     * */
#define STRINGIFY_EXPAND(s) STRINGIFY(s)
    /**
     * @brief   append a sharp mark to a symbol
     * */
#define STRINGIFY(s) #s

    namespace misc_utils {
        /**
         * @brief   get the maximum value for a given value type
         * @note the type given should be numeric
         * */
        template<typename t_type>
        t_type get_max_t_val(t_type t) {
            return (std::numeric_limits<t_type>::max)();
        }


        /**
         * @brief move a given iterator forward by a specific offset
         * @param it    iterator to advance
         * @param count offset to advance
         * @return  the iterator ahead the given iterator by the specified offset
         * */
        template<typename t_iterator>
        t_iterator move_it_forward(t_iterator it, size_t count) {
            while (count--)
                it++;
            return it;
        }

        /**
         * @brief move a given iterator backward by a specific offset
         * @param it    iterator to decrement
         * @param count offset to decrement
         * @return  the iterator behind the given iterator by the specified offset
         * */
        template<typename t_iterator>
        t_iterator move_it_backward(t_iterator it, size_t count) {
            while (count--)
                it--;
            return it;
        }


        // TEMPLATE STRUCT less
        /**
         * @brief   a functor acting as binary less-than comparator based on
         *          the memory bytes of its POD operands
         * */
        template<class _Ty>
        struct less_as_pod
                : public std::binary_function<_Ty, _Ty, bool> {
            // functor for operator<
            bool operator()(const _Ty &_Left, const _Ty &_Right) const {
                // apply operator< to operands
                return memcmp(&_Left, &_Right, sizeof(_Left)) < 0;
            }
        };

        /**
         * @brief  less-than comparator function based on the memory bytes of
         *          the given POD operands
         * */
        template<class _Ty>
        bool is_less_as_pod(const _Ty &_Left, const _Ty &_Right) {
            // apply operator< to operands
            return memcmp(&_Left, &_Right, sizeof(_Left)) < 0;
        }


        /**
         * @brief ask this thread to sleep for a period
         * @param ms    number of milliseconds to sleep
         * @return dummy `true`
         * */
        inline
        bool sleep_no_w(long ms) {
            boost::this_thread::sleep(
                    boost::get_system_time() +
                    boost::posix_time::milliseconds(std::max<long>(ms, 0)));

            return true;
        }

        /**
         * @brief   estimate the median value for sortable vector
         * @details     Sort the vector first, and then return the middle element
         *              in case of #(elements) is odd, and the average of the two
         *              middle values
         * @param v     vector of sortable elements
         * @return median of the elements
         * */
        template<class type_vec_type>
        type_vec_type median(std::vector <type_vec_type> &v) {
            if (v.empty())
                return boost::value_initialized<type_vec_type>();
            if (v.size() == 1)
                return v[0];

            size_t n = (v.size()) / 2;
            std::sort(v.begin(), v.end());
            if (v.size() % 2) {//1, 3, 5...
                return v[n];
            } else {//2, 4, 6...
                return (v[n - 1] + v[n]) / 2;
            }

        }

        /**
         * @brief an abstract wrapper for callback function during destruction
         * */
        struct call_befor_die_base {
            virtual ~call_befor_die_base() {}
        };

        /**
         * @brief   type definition for a shared pointer for `call_befor_die_base`
         * */
        typedef boost::shared_ptr <call_befor_die_base> auto_scope_leave_caller;


        /**
         * @brief   a wrapper of a callback function before its destruction
         * @note    `t_scope_leave_handler` should be a functor
         * */
        template<class t_scope_leave_handler>
        struct call_befor_die : public call_befor_die_base {
            t_scope_leave_handler m_func;

            /**
             * @brief   make a wrapper with given handler
             * @param f functor for callback during destruction
             * */
            call_befor_die(t_scope_leave_handler f) : m_func(f) {}

            /**
             * @brief   a destructor that calls the underlying functor before destruction
             * */
            ~call_befor_die() {
                m_func();
            }
        };

        /**
         * @brief   make handler containing a callback for exiting its scope
         * @param f functor acting as the callback
         * @return  pointer to the resultant handler
         * */
        template<class t_scope_leave_handler>
        auto_scope_leave_caller create_scope_leave_handler(t_scope_leave_handler f) {
            auto_scope_leave_caller slc(new call_befor_die<t_scope_leave_handler>(f));
            return slc;
        }

        /**
         * @brief   erase element satisfying the given predicate from the container
         * @param items a container of elements to check
         * @param predicate condition for the erase
         * */
        template<typename t_contaner, typename t_redicate>
        void erase_if(t_contaner &items, const t_redicate &predicate) {
            for (auto it = items.begin(); it != items.end();) {
                if (predicate(*it))
                    it = items.erase(it);
                else
                    ++it;
            }
        };
    } // namespace misc_utils
} // namespace epee
