// Copyright (c) 2006-2013, Andrey N. Sabelnikov, www.sabelnikov.net
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// * Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer.
// * Redistributions in binary form must reproduce the above copyright
// notice, this list of conditions and the following disclaimer in the
// documentation and/or other materials provided with the distribution.
// * Neither the name of the Andrey N. Sabelnikov nor the
// names of its contributors may be used to endorse or promote products
// derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER  BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

/**
*@file  console_handler.h
*@brief   a utility class serves to handling standard input
*@version   1.0, 2017-07-08
*/
#pragma once

#include <atomic>
#include <condition_variable>
#include <functional>
#include <mutex>
#include <thread>

// add by @sammietocat at 2017-07-08
#include "misc_log_ex.h"

namespace epee {
/**
*@brief     an aysnchronous reader for standard input
*/
class async_stdin_reader {
  public:
    /**
    *@brief   constructor for initialization
    *@details     several jobs involved are
                      + start the reader
                      + signal to wait for reading request
                      + bind the reading thread to member function
                        {@link reader_thread_func}

    */
    async_stdin_reader()
        : m_run( true )
        , m_has_read_request( false )
        , m_read_status( state_init ) {
        m_reader_thread = std::thread(
            std::bind( &async_stdin_reader::reader_thread_func, this ) );
    }
    /**
    *@brief     destructor would stop the reader and quit
    */
    ~async_stdin_reader() { stop(); }

    // Not thread safe. Only one thread can call this method at once.
    bool get_line( std::string &line ) {
        if ( !start_read() ) return false;

        std::unique_lock<std::mutex> lock( m_response_mutex );
        while ( state_init == m_read_status ) {
            m_response_cv.wait( lock );
        }

        bool res = false;
        if ( state_success == m_read_status ) {
            line = m_line;
            res  = true;
        }

        m_read_status = state_init;

        return res;
    }
    /**
    *@brief     stop the reader and quit
    */
    void stop() {
        if ( m_run ) {
            m_run.store( false, std::memory_order_relaxed );

#if defined( WIN32 )
            ::CloseHandle(::GetStdHandle( STD_INPUT_HANDLE ) );
#endif

            m_request_cv.notify_one();
            // join the underlying reader thread for exit
            m_reader_thread.join();
        }
    }

  private:
    /**
    *@brief ask for a read
    *@return `true` if the request is accepted. Otherwise, `false`
    */
    bool start_read() {
        std::unique_lock<std::mutex> lock( m_request_mutex );
        if ( !m_run.load( std::memory_order_relaxed ) || m_has_read_request )
            return false;

        m_has_read_request = true;
        m_request_cv.notify_one();
        return true;
    }
    /**
    *@brief check the status of the reader
    *@return `true` if the reader is running and
                there is no reading request to handle.
            `false`, otherwise
    */
    bool wait_read() {
        std::unique_lock<std::mutex> lock( m_request_mutex );
        while ( m_run.load( std::memory_order_relaxed ) &&
                !m_has_read_request ) {
            m_request_cv.wait( lock );
        }

        if ( m_has_read_request ) {
            m_has_read_request = false;
            return true;
        }

        return false;
    }

    /**
    *@brief ask for a reading operation
    *@return    `true` if the reader is ready. Otherwise, `false`
    */
    bool wait_stdin_data() {
#if !defined( WIN32 )
        int stdin_fileno = ::fileno( stdin );

        while ( m_run.load( std::memory_order_relaxed ) ) {
            fd_set read_set;
            FD_ZERO( &read_set );
            FD_SET( stdin_fileno, &read_set );

            struct timeval tv;
            tv.tv_sec  = 0;
            tv.tv_usec = 100 * 1000;

            // try to read a line
            int retval =
                ::select( stdin_fileno + 1, &read_set, NULL, NULL, &tv );
            if ( retval < 0 )
                return false;
            else if ( 0 < retval )
                return true;
        }
#endif

        return true;
    }

    /**
    *@brief     function to handle reading request
    *@details   It would keep running until the reader is permitted to run.
                And during each reading request, it will
                    + check if the reader is still ok to run
                    + read and validate a line
                    + update the line read in
    */
    void reader_thread_func() {
        while ( true ) {
            // exit if the reader is stopped
            if ( !wait_read() ) break;

            std::string line;
            bool        read_ok = true;
            // try to read a line
            if ( wait_stdin_data() ) {
                if ( m_run.load( std::memory_order_relaxed ) ) {
                    // read in a line
                    std::getline( std::cin, line );
                    // ensure it's no EOF flag or failure
                    read_ok = !std::cin.eof() && !std::cin.fail();
                }
            } else {
                read_ok = false;
            }

            {
                std::unique_lock<std::mutex> lock( m_response_mutex );
                if ( m_run.load( std::memory_order_relaxed ) ) {
                    // move the line read in to the data member `m_line`
                    m_line = std::move( line );
                    // update the reading status flag
                    m_read_status = read_ok ? state_success : state_error;
                } else {
                    m_read_status = state_cancelled;
                }
                m_response_cv.notify_one();
            }
        }
    }

    /**
    *@brief     enumeration of reading status
    */
    enum t_state { state_init, state_success, state_error, state_cancelled };

  private:
    std::thread       m_reader_thread;
    std::atomic<bool> m_run; ///< running status of the reader, `true` means
                             /// running. Otherwise, `false`

    std::string m_line;             ///< last command
    bool        m_has_read_request; // flag indicating if more request to handle
    t_state     m_read_status;      // status of a reading operation

    std::mutex              m_request_mutex;  ///< mutex for reading request
    std::mutex              m_response_mutex; ///< mutex for reading response
    std::condition_variable m_request_cv;
    std::condition_variable m_response_cv;
};

template <class t_server>
bool empty_commands_handler( t_server *psrv, const std::string &command ) {
    return true;
}

class async_console_handler {
  public:
    /**
    *@brief     a dummy default constructor
    */
    async_console_handler() {}

    template <class t_server, class chain_handler>
    bool run( t_server *psrv, chain_handler ch_handler,
              const std::string &prompt = "#", const std::string &usage = "" ) {
        return run(
            prompt, usage,
            [&]( const std::string &cmd ) { return ch_handler( psrv, cmd ); },
            [&] { psrv->send_stop_signal(); } );
    }

    /**
    *@brief     kickstart a given handler
    *@param ch_handler  handler to run
    *@param prompt  optional prompt string
    *@param usage   optional description of usage
    *@return    `true` in case of successful kickstart. Otherwise, `false`
    */
    template <class chain_handler>
    bool run( chain_handler ch_handler, const std::string &prompt = "#",
              const std::string &usage = "" ) {
        return run( prompt, usage,
                    [&]( const std::string &cmd ) { return ch_handler( cmd ); },
                    [] {} );
    }

    /**
    *@brief     stop the underlying reader for reading command from `stdin`
    */
    void stop() { m_stdin_reader.stop(); }

  private:
    /**
    *@brief     process each given command from the standard input
    *@param prompt  a message for prompt
    *@param usage   description of the usage of running program
    *@param cmd_handler function to handle the input command
    *@param exit_handler    the callback function for program exit
    *@return `true` in case of no errors. Otherwise, false
    */
    template <typename t_cmd_handler, typename t_exit_handler>
    bool run( const std::string &prompt, const std::string &usage,
              const t_cmd_handler & cmd_handler,
              const t_exit_handler &exit_handler ) {
        TRY_ENTRY();
        bool continue_handle = true;
        while ( continue_handle ) {
            // print the prompt in yellow
            // and append a tail space if necessary
            if ( !prompt.empty() ) {
                epee::log_space::set_console_color(
                    epee::log_space::console_color_yellow, true );
                std::cout << prompt;
                if ( ' ' != prompt.back() ) std::cout << ' ';
                epee::log_space::reset_console_color();
                std::cout.flush();
            }

            std::string command;
            // read in a command fro `stdin`
            if ( !m_stdin_reader.get_line( command ) ) {
                LOG_PRINT( "Failed to read line. Stopping...", LOG_LEVEL_0 );
                continue_handle = false;
                break;
            }
            // trim the leading the tailing spaces
            string_tools::trim( command );

            LOG_PRINT_L2( "Read command: " << command );
            if ( 0 == command.compare( "exit" ) ||
                 0 == command.compare( "q" ) ) {
                continue_handle = false;
            } else if ( !command.compare( 0, 7, "set_log" ) ) {
                // parse set_log command
                if ( command.size() != 9 ) {
                    std::cout << "wrong syntax: " << command << std::endl
                              << "use set_log n" << std::endl;
                    continue;
                }
                uint16_t n = 0;
                // extract the log level as a integer
                if ( !string_tools::get_xtype_from_string(
                         n, command.substr( 8, 1 ) ) ) {
                    std::cout << "wrong syntax: " << command << std::endl
                              << "use set_log n" << std::endl;
                    continue;
                }
                log_space::get_set_log_detalisation_level( true, n );
                std::cout << "New log level set " << n;
                LOG_PRINT_L2( "New log level set " << n );
            } else if ( command.empty() ) {
                // simply skip for empty line
                continue;
            } else if ( cmd_handler( command ) ) {
                // pass the command to the handler
                continue;
            } else {
                // in case of no handler binding with command
                std::cout << "unknown command: " << command << std::endl;
                std::cout << usage;
            }
        }
        // don't forget the callback when exit
        exit_handler();
        return true;
        CATCH_ENTRY_L0( "console_handler", false );
    }

  private:
    async_stdin_reader
        m_stdin_reader; ///< reader serves to read command from `stdin`
};

template <class t_server, class t_handler>
bool start_default_console( t_server *ptsrv, t_handler handlr,
                            const std::string &prompt,
                            const std::string &usage = "" ) {
    std::shared_ptr<async_console_handler> console_handler =
        std::make_shared<async_console_handler>();
    boost::thread( [=]() {
        console_handler->run<t_server, t_handler>( ptsrv, handlr, prompt,
                                                   usage );
    } ).detach();
    return true;
}

template <class t_server>
bool start_default_console( t_server *ptsrv, const std::string &prompt,
                            const std::string &usage = "" ) {
    return start_default_console( ptsrv, empty_commands_handler<t_server>,
                                  prompt, usage );
}

template <class t_server, class t_handler>
bool no_srv_param_adapter( t_server *ptsrv, const std::string &cmd,
                           t_handler handlr ) {
    return handlr( cmd );
}

template <class t_server, class t_handler>
bool run_default_console_handler_no_srv_param( t_server *         ptsrv,
                                               t_handler          handlr,
                                               const std::string &prompt,
                                               const std::string &usage = "" ) {
    async_console_handler console_handler;
    return console_handler.run(
        ptsrv, boost::bind<bool>( no_srv_param_adapter<t_server, t_handler>, _1,
                                  _2, handlr ),
        prompt, usage );
}

template <class t_server, class t_handler>
bool start_default_console_handler_no_srv_param(
    t_server *ptsrv, t_handler handlr, const std::string &prompt,
    const std::string &usage = "" ) {
    boost::thread( boost::bind(
        run_default_console_handler_no_srv_param<t_server, t_handler>, ptsrv,
        handlr, prompt, usage ) );
    return true;
}

/**
*@brief     implementation of a binder for linking commands
            from `stdin` to callback functions
*/
class console_handlers_binder {
    // type definition of function accepting of a vector of string
    // and return a boolean value
    typedef boost::function<bool( const std::vector<std::string> & )>
        console_command_handler;
    // a (commandName, ({@link console_command_handler},description))
    // two-level map
    typedef std::map<std::string,
                     std::pair<console_command_handler, std::string>>
                                   command_handlers_map;
    std::unique_ptr<boost::thread> m_console_thread;
    command_handlers_map           m_command_handlers; ///< handler map
    async_console_handler m_console_handler; ///< a asynchronous console handler
                                             /// detailed as {@link
                                             /// async_console_handler}

  public:
    /**
    *@brief     produce a message explaining the usage of the executable
    *@return    the usage message, each line of which takes form of
                `commandName   description`
    */
    std::string get_usage() {
        std::stringstream ss;
        size_t            max_command_len = 0;
        for ( auto &x : m_command_handlers )
            if ( x.first.size() > max_command_len )
                max_command_len = x.first.size();

        for ( auto &x : m_command_handlers ) {
            ss.width( max_command_len + 3 );
            ss << std::left << x.first << x.second.second << ENDL;
        }
        return ss.str();
    }
    /**
    *@brief     add a handler for a command with optional description
    *@param cmd     name of command of add
    *@param hndlr   handler to bind with `cmd`, detailed as
                    {@link console_command_handler}
    *@param usage   description of the usage of `cmd`
    */
    void set_handler( const std::string &            cmd,
                      const console_command_handler &hndlr,
                      const std::string &            usage = "" ) {
        command_handlers_map::mapped_type &vt = m_command_handlers[ cmd ];
        vt.first                              = hndlr;
        vt.second                             = usage;
    }
    /**
    *@brief     process a given argument vector
    *@details   e.g., given cmd as {"hello", "arg1", "arg2"},
                we would execute program `hello` with
                arguments `arg1` and `arg2`
    *@param cmd     argument vector to process
    *@return `true` means no errors occur during processing.
                `false`, otherwise.
    */
    bool process_command_vec( const std::vector<std::string> &cmd ) {
        // check if the arguments vector (including the command name)
        // is empty
        if ( !cmd.size() ) return false;
        // extract the command name
        auto it = m_command_handlers.find( cmd.front() );
        // check if the command has been bound
        if ( it == m_command_handlers.end() ) return false;
        // extract the arguments vector without command name
        std::vector<std::string> cmd_local( cmd.begin() + 1, cmd.end() );
        // execute the command with the given arguments
        return it->second.first( cmd_local );
    }
    /**
    *@brief     take a command string and process it
    *@param cmd command string to process including the program
    *@return status of processing. `true` means successful.
            `false`, otherwise.
    */
    bool process_command_str( const std::string &cmd ) {
        std::vector<std::string> cmd_v;
        // split the given string into argument vector
        boost::split( cmd_v, cmd, boost::is_any_of( " " ),
                      boost::token_compress_on );
        // delegate to {@link process_command_vec}
        return process_command_vec( cmd_v );
    }
    /**
    *@brief     bind the member function {@link running_handling}
                to handle commands
    *@deteils   the command handling will be run on a detached thread
    *@param prompt  message display before starting the handling
    *@param usage_string    description of the usage of the running program
    *@return `true`
    */
    bool start_handling( const std::string &prompt,
                         const std::string &usage_string = "" ) {
        m_console_thread.reset( new boost::thread(
            boost::bind( &console_handlers_binder::run_handling, this, prompt,
                         usage_string ) ) );
        m_console_thread->detach();
        return true;
    }
    /**
    *@brief     starting the underlying console handler
    */
    void stop_handling() { m_console_handler.stop(); }
    /**
    *@brief     start to handle commands input from console
    *@details   the actual job is delegated to
                {@link async_console_handler::run()}
    *@return    `true` if the underlying handler utility is up. Otherwise,
    *`false`
    */
    bool run_handling( const std::string &prompt,
                       const std::string &usage_string ) {
        return m_console_handler.run(
            boost::bind( &console_handlers_binder::process_command_str, this,
                         _1 ),
            prompt, usage_string );
    }
};

/* work around because of broken boost bind */
template <class t_server>
class srv_console_handlers_binder : public console_handlers_binder {
    bool process_command_str( t_server * /*psrv*/, const std::string &cmd ) {
        return console_handlers_binder::process_command_str( cmd );
    }

  public:
    bool start_handling( t_server *psrv, const std::string &prompt,
                         const std::string &usage_string = "" ) {
        boost::thread(
            boost::bind( &srv_console_handlers_binder<t_server>::run_handling,
                         this, psrv, prompt, usage_string ) )
            .detach();
        return true;
    }

    bool run_handling( t_server *psrv, const std::string &prompt,
                       const std::string &usage_string ) {
        return m_console_handler.run(
            psrv,
            boost::bind(
                &srv_console_handlers_binder<t_server>::process_command_str,
                this, _1, _2 ),
            prompt, usage_string );
    }

    void stop_handling() { m_console_handler.stop(); }

  private:
    async_console_handler m_console_handler;
};
} // namespace epee