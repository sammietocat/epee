var indexSectionsWithContent =
{
  0: "_abcdefghijklmnopqrstuvwxz~",
  1: "abcdefghiklmnoprstu",
  2: "ems",
  3: "acefghiklmnprstwz",
  4: "_abcdefghiklmnopqrstuwx~",
  5: "abcdeghijlmnoprstuv",
  6: "abcefhlmnpstuv",
  7: "bchmorst",
  8: "chors",
  9: "acil",
  10: "abcdefghiklmnoprstw"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "typedefs",
  7: "enums",
  8: "enumvalues",
  9: "related",
  10: "defines"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Variables",
  6: "Typedefs",
  7: "Enumerations",
  8: "Enumerator",
  9: "Friends",
  10: "Macros"
};

