# A developement of epee library [2017-07-02]
<!-- TOC -->
## Outline  
- [Overview](#overview)
- [How to build](#how-to-build)
    - [Environment](#environment)
- [Examples](#examples)
- [Roadmap](#roadmap)

<!-- /TOC -->
## Overview  
This project serves to help someone to learn basic APIs of the [epee](https://github.com/sabelnikov/epee) library.  

## How to build  
### Environment  
+ g++ (GCC) 6.3.1 20161221 (Red Hat 6.3.1-1)  
+ boost libraries 1.60  

TBA...  

## Examples  
TBA...  
There are some refactored examples (not documented yet) under the `example` folder.  

## Roadmap  
+ WIP: Refactor all the platform-dependent interfaces to the platform-independent ones

